package com.example.milani.lines.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by milani on 26.6.18..
 */

public class Piece {

    Bitmap bitmap;
    int bitmapIndex;

    public Piece(Bitmap bitmap, int bitmapIndex){
        this.bitmap = bitmap;
        this.bitmapIndex = bitmapIndex;
    }

}
