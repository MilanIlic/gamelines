package com.example.milani.lines.game;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by milani on 26.6.18..
 */

public class GameController {


    public interface ViewInterface {
        void updateImage();
        void addScore(int points);
        void updateNewPieces();
    }

    private ViewInterface view;
    private ImageData imageData;

    int points;

    public GameController(ViewInterface gameActivity, ImageData imageData){
        this.view = gameActivity;
        this.imageData = imageData;
        view.updateNewPieces();
    }


    public void down(float x, float y) {
        int positionI = imageData.getPositionI(y);
        int positionJ = imageData.getPositionJ(x);

        if (positionI >= 9 || positionJ >= 9)
            return;

        Log.d("GameController: SELECT", "Selected: [ " + positionI + ", " + positionJ + " ]");

        if(imageData.hasSelected()){
            if(imageData.getField(positionI, positionJ).piece != null) // if dest field already has piece, select it
                imageData.selectPiece(positionI, positionJ);
            else {
                // try to find the shortest path
                int startI = imageData.getSelected().getI(), startJ = imageData.getSelected().getJ();
                int destI = positionI, destJ = positionJ;

                List<Pair<Integer, Integer>> path = findPath(startI, startJ, destI, destJ);
                if(path.size() == 0)
                    Toast.makeText((Context)view, "There is no path to selected field!", Toast.LENGTH_SHORT).show();
                else {
                    for(Pair p: path){
                        Log.d("PATH", "[ " + p.first + ", " + p.second + " ];");
                    }
                    simulateMoving(startI, startJ, path);
                    imageData.resetSelected();
                    view.addScore(points);
                }
            }
        }else{
            imageData.selectPiece(positionI, positionJ);
        }

        view.updateImage();
    }

    private void simulateMoving(int startI, int startJ, List<Pair<Integer, Integer>> path) {
        (new AsyncTask<Object, Integer, Void>(){
            @Override
            protected Void doInBackground(Object... positions) {
                super.onPreExecute();
                int startI = (Integer)positions[0], startJ = (Integer)positions[1];
                List<Pair<Integer, Integer>> path = (List<Pair<Integer, Integer>>)positions[2];
                Log.d("TAG", "SIMULATION HAS STARTED");
                try {
                    for (Pair<Integer, Integer> pair : path) {
                        int destI = pair.first, destJ = pair.second;

                        Log.d("SIMULATION: ", "Moving from: [ " + startI + ", " + startJ + " ] ====> [ " + destI + ", " + destJ + " ];");
                        imageData.moveFigure(startI, startJ, destI, destJ);
                        view.updateImage();
                        Thread.sleep(100);

                        startI = destI;
                        startJ = destJ;
                    }
                }catch (InterruptedException ie){
                    ie.printStackTrace();
                }

                Log.d("TAG", "SIMULATION HAS FINISHED");

                if(!checkPairs()) {
                    imageData.addNewPieces();
                    checkPairs();
                }

                view.updateImage();

                return null;
            }

            @Override
            public void onPostExecute(Void obj){
                view.addScore(points);
                view.updateNewPieces();
                //imageData.debugMatrix();
                view.updateImage();
                checkIsGameFinished();
            }
        }).execute(startI, startJ, path);
    }

    private void checkIsGameFinished() {
        if(imageData.getPiecesCount() == 81){
            Dialog.setScore(points);
            Dialog.show();
        }
    }

    public void cancel() {

    }

    public static final int ACROSS = 0, DOWN = 1;

    private boolean checkPairs(){
        int newPoints = 0;
        int count = 0;
        int currentIndex = -1;
        Field[][] fields = imageData.getFields();

        // CHECK ALL ROWS
        for(int i = 0; i<9; i++){
            count = 0;
            currentIndex = -1;
            for(int j = 0; j<9; j++){

                if(fields[i][j].getPiece() != null){

                    Piece piece = fields[i][j].getPiece();
                    if(piece.bitmapIndex != currentIndex){
                        if(count >= 5) {
                            newPoints += count * 10;
                            removePairs(ACROSS, i, j-1, count); // remove all concatenated pieces in the same row
                        }
                        currentIndex = piece.bitmapIndex;
                        count = 1;
                    }else
                        count++;
                }else{
                    if(count >= 5) {
                        newPoints += count * 10;
                        removePairs(ACROSS, i, j - 1, count); // remove all concatenated pieces in the same row
                    }
                    currentIndex = -1;
                    count = 0;
                }
            }
            if(count >= 5) {
                newPoints += count * 10;
                removePairs(ACROSS, i, 8, count); // remove all concatenated pieces in the same row
            }
        }

        // CHECK ALL COLUMNS
        for(int j = 0; j<9; j++){
            count = 0;
            currentIndex = -1;
            for(int i = 0; i<9; i++){

                if(fields[i][j].getPiece() != null){

                    Piece piece = fields[i][j].getPiece();
                    if(piece.bitmapIndex != currentIndex){
                        if(count >= 5) {
                            newPoints += count * 10;
                            removePairs(DOWN, i-1, j, count); // remove all concatenated pieces in the same column
                        }
                        currentIndex = piece.bitmapIndex;
                        count = 1;
                    }else
                        count++;
                }else{
                    if(count >= 5) {
                        newPoints += count * 10;
                        removePairs(DOWN, i - 1, j, count); // remove all concatenated pieces in the same column
                    }
                    currentIndex = -1;
                    count = 0;
                }
            }
            if(count >= 5) {
                newPoints += count * 10;
                removePairs(DOWN, 8, j, count); // remove all concatenated pieces in the same column
            }
        }

        points += newPoints;
        Log.d("SCORE", "Points: " + points);

        return newPoints > 0;
    }

    private boolean isCrossed(int checkDirection, int startI, int startJ, int pieceType){
        int count = 0;
        Field[][] fields = imageData.getFields();

        if(checkDirection == ACROSS){
            // direction left
            for(int j = startJ + 1; j < 9 && fields[startI][j].hasSamePieceType(pieceType); j++) count++;
            // direction right
            for(int j = startJ - 1; j >= 0 && fields[startI][j].hasSamePieceType(pieceType); j--) count++;

        }else{ // DOWN
            // direction down
            for(int i = startI + 1; i < 9 && fields[i][startJ].hasSamePieceType(pieceType); i++) count++;
            // direction up
            for(int i = startI - 1; i >= 0 && fields[i][startJ].hasSamePieceType(pieceType); i--) count++;
        }

        Log.d("REMOVE", "Crossed count: " + count);
        return count >= 4;
    }


    private void removePairs(int direction, int startI, int startJ, int count){
        Log.d("REMOVE", "Removing pairs! " + direction + "  count: " +count);
        Field[][] fields = imageData.getFields();

        if(direction == DOWN){ // remove the all concatenated pieces in the same column
            for(int i = startI; i >= 0 && count>0; i--, count--){
                if(!isCrossed(ACROSS, i, startJ, fields[i][startJ].getPiece().bitmapIndex)) {
                    imageData.removePiece(i, startJ);
                }
            }
        }else{ // ACROSS ---> remove the all concatenated pieces in the same row
            for(int j = startJ; j >= 0 && count>0; j--, count--){
                if(!isCrossed(DOWN, startI, j, fields[startI][j].getPiece().bitmapIndex)) {
                    imageData.removePiece(startI, j);
                }
            }
        }

    }

    private boolean isCoordinateValid(int x, int y){
        return (x >= 0 && x < 9) &&
                (y >= 0 && y < 9);
    }

    public List<Pair<Integer, Integer>> findPath(int srcI, int srcJ, int destI, int destJ){
        Log.d("TAG", "Looking for path between: [ " + srcI + ", " + srcJ + " ] and " + "[ " + destI + ", " + destJ + " ];");
        Field[][] fields = imageData.getFields();
        // matrix --> true  : field is empty
        // matrix --> false  : field has piece

        boolean[][] matrix = new boolean[9][9];
        boolean[][] visited = new boolean[9][9];

        // these are used to get 4 neighbours fields
        int rowNum[] = {-1, 0, 0, 1};
        int colNum[] = {0, -1, 1, 0};

        if(srcI == destI && srcJ == destJ) return new ArrayList<>(); // the same field


        for(int i = 0; i<9; i++)
            for(int j = 0; j<9; j++)
                if(fields[i][j].getPiece() == null)
                    matrix[i][j] = true;

        if(matrix[srcI][srcJ] || // source field don't have a piece
                !matrix[destI][destJ]) // dest field already has a piece
            return new ArrayList<>();

        visited[srcI][srcJ] = true;

        List<Pair<Integer, Integer>> queue = new LinkedList<>();
        HashMap<Pair<Integer, Integer>, ArrayList<Pair<Integer, Integer>>> pathSolutions = new HashMap<>();

        Pair<Integer, Integer> startField = new Pair(srcI, srcJ);
        queue.add(startField); // add start point
        pathSolutions.put(startField, new ArrayList<Pair<Integer, Integer>>());

        while(queue.size() > 0){
            Pair<Integer, Integer> field = queue.remove(0);
            ArrayList<Pair<Integer, Integer>> currentPath = pathSolutions.get(field);

            Log.d("TAG", "Current field: [ " + field.first + ", " + field.second + " ];");
            if(field.first == destI && field.second == destJ)
                return pathSolutions.get(field);

            for(int i = 0; i<4; i++){
                int row = field.first + rowNum[i];
                int col = field.second + colNum[i];

                Log.d("TAG", "==> Try field: [ " + row + ", " + col + " ];");
                if(isCoordinateValid(row, col) &&
                        matrix[row][col] && // field is empty
                        !visited[row][col]) { // field hasn't been visited yet

                    visited[row][col] = true;
                    Pair<Integer, Integer> neighbour = new Pair<>(row, col);
                    queue.add(neighbour);

                    ArrayList<Pair<Integer, Integer>> path = new ArrayList<>(currentPath);
                    path.add(neighbour);
                    pathSolutions.put(neighbour, path);
                }

            }
        }

        return new ArrayList<>();
    }


    public int getPoints() { return points; }

}
