package com.example.milani.lines.game;

import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by milani on 26.6.18..
 */

public class BoardImageView extends android.support.v7.widget.AppCompatImageView {
    ImageData imageData;

    public BoardImageView(Context context) {
        super(context);
    }

    public BoardImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BoardImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    void init() {
        imageData = new ImageData(getLeft(), getTop(), getResources());
    }

    public ImageData getImageData() { return imageData; }

    @Override
    public void onSizeChanged(int w, int h, int oldW, int oldH){
        imageData.onSizeChanged(w, h);
        invalidate();
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);

        for(int i = 0; i<9; i++)
            for(int j = 0; j<9; j++)
                imageData.matrix[i][j].draw(canvas);
        // draw pieces

        // draw selected field

    }
}
