package com.example.milani.lines.game;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

import com.example.milani.lines.R;

import java.util.Random;

/**
 * Created by milani on 26.6.18..
 */

public class ImageData {

    public Field[][] matrix;
    float imageLeft, imageTop, imageRight, imageBottom;
    float imageWidth, imageHeight;

    double leftPaddingPct = 0.0357142857143;
    double topPaddingPct = 0.03125;
    double figureSizePct = 0.1038;//64285714;

    Field selectedField;
    int piecesCount = 6;

    // used for next 3 figures preview
    Piece[] nextPieces;

    static Bitmap king, queen, rock, pawn, bishop;

    public ImageData(float imageLeft, float imageTop, Resources resources){

        // get image position
        this.imageLeft = imageLeft;
        this.imageTop = imageTop;

        // init fields
        matrix = new Field[9][9];
        for(int i = 0; i<9; i++)
            for(int j = 0; j<9; j++)
                matrix[i][j] = new Field(i, j);

        // get figure images
        king = BitmapFactory.decodeResource(resources, R.drawable.king);
        queen = BitmapFactory.decodeResource(resources, R.drawable.queen);
        rock = BitmapFactory.decodeResource(resources, R.drawable.rock);
        bishop = BitmapFactory.decodeResource(resources, R.drawable.bishop);
        pawn = BitmapFactory.decodeResource(resources, R.drawable.pawn);

        // set initial pieces
        for(int i = 0; i<piecesCount; i++){
            getRandomEmptyField().setPiece(getRandomPiece());
        }

        // prepare next 3 pieces
        nextPieces = new Piece[3];
        prepare3NewPieces();

    }

    private void prepare3NewPieces(){
        for(int i = 0; i<3; i++) nextPieces[i] = getRandomPiece();
    }

    private Piece getRandomPiece(){
        Bitmap[] bitmaps = { king, queen, rock, bishop, pawn };
        Random random = new Random();
        int pieceBitmapIndex = random.nextInt(bitmaps.length);

        return new Piece(bitmaps[pieceBitmapIndex], pieceBitmapIndex);
    }


    private Field getRandomEmptyField(){
        if(piecesCount == 81)
            return null; // the board is full

        Random random = new Random();

        while(true) {
            int row = random.nextInt(9), col = random.nextInt(9);

            if (matrix[row][col].piece == null)
                return matrix[row][col];
        }
    }

    public void onSizeChanged(int w, int h) {
        imageWidth = w;
        imageHeight = h;

        updateFieldSize();
    }

    private void updateFieldSize() {
        double leftPadding = imageWidth * leftPaddingPct;
        double topPadding = imageHeight * topPaddingPct;
        double figureWidth = imageWidth * figureSizePct;
        double figureHeight = imageHeight * figureSizePct;

        for(int i = 0; i<9; i++)
            for(int j = 0; j<9; j++) {
                float left = (float)(leftPadding + j*figureWidth), right = (float)(left + figureWidth);
                float top = (float)(imageTop + topPadding + i*figureHeight), bottom = (float)(top + figureHeight);

                matrix[i][j].setPosition(new RectF(left, top, right, bottom));
            }
    }


    public int getPositionI(float y) {
        double topPadding = imageHeight * topPaddingPct;
        double figureHeight = imageHeight * figureSizePct;

        y -= (imageTop + topPadding);

        return (int)(y/figureHeight);
    }

    public int getPositionJ(float x) {
        double leftPadding = imageWidth * leftPaddingPct;
        double figureWidth = imageWidth * figureSizePct;

        x -= (imageLeft + leftPadding);

        return (int)(x/figureWidth);
    }

    public boolean hasSelected() {
        return selectedField != null;
    }

    public void selectPiece(int positionI, int positionJ) {
        if(matrix[positionI][positionJ].piece != null) {
            if(selectedField != null)
                selectedField.selected = false;
            selectedField = matrix[positionI][positionJ];
            selectedField.selected = true;
        }
    }

    public Field getField(int positionI, int positionJ){
        if(positionI >= 0 && positionI < 9 && positionJ >= 0 && positionJ < 9){
            return matrix[positionI][positionJ];
        }
        return null;
    }

    public Field[][] getFields() {
        return matrix;
    }

    public Field getSelected() {
        return selectedField;
    }

    public void moveFigure(int startI, int startJ, int destI, int destJ) {
        matrix[destI][destJ].piece = matrix[startI][startJ].piece;
        matrix[startI][startJ].piece = null;

        //debugMatrix();
    }

    public void debugMatrix(){
        System.out.println("\n==============\n");
        for(int i = 0; i<9; i++) {
            System.out.println();
            for (int j = 0; j < 9; j++) {
                if(matrix[i][j].piece == null)
                    System.out.print(" 0 ");
                else
                    System.out.print(" 1 ");
            }
        }
        System.out.println("\n==============\n");
    }

    public void addNewPieces() {
        int newPiecesNum = 3;

        while(newPiecesNum > 0 && piecesCount < 81){

            if(getRandomEmptyField().setPiece(nextPieces[newPiecesNum - 1])) {
                newPiecesNum--;
                piecesCount++;
            }
        }
        prepare3NewPieces();
    }

    public void resetSelected() {
        if(selectedField != null){
            selectedField.selected = false;
            selectedField = null;
        }
    }

    public Bitmap getNextPiece(int i) {
        return nextPieces[i].bitmap;
    }

    public int getPiecesCount() {
        return piecesCount;
    }

    public void removePiece(int i, int j) {
        matrix[i][j].piece = null;
        piecesCount--;
    }
}
