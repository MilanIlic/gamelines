package com.example.milani.lines;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.milani.lines.game.BoardImageView;
import com.example.milani.lines.game.Dialog;
import com.example.milani.lines.game.GameController;
import com.example.milani.lines.game.ImageData;

public class GameActivity extends AppCompatActivity implements View.OnTouchListener, GameController.ViewInterface{

    GameController gameController;
    BoardImageView imageView;
    ImageData imageData;
    SharedPreferences sharedPref;
    int highScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        init();
    }

    private void init(){
        sharedPref = getPreferences(Context.MODE_PRIVATE);

        // fetch high score
        highScore = sharedPref.getInt(getString(R.string.high_score), 0);
        ((TextView)(findViewById(R.id.scoreBestText))).setText("Best: " + highScore);

        imageView = findViewById(R.id.imageView);
        imageView.setOnTouchListener(this);
        imageData = imageView.getImageData();
        gameController = new GameController(this, imageData);

        // init dialog
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(GameActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.game_dialog, null);
        mBuilder.setView(mView);

        Dialog.scoreTextView = (TextView) mView.findViewById(R.id.dialogScoreText);
        Dialog.tryAgainButton = (Button) mView.findViewById(R.id.tryAgainButton);
        Dialog.quitButton = (Button) mView.findViewById(R.id.quitButton);
        Dialog.setDialog(mBuilder.create());

        Dialog.dialog.setCanceledOnTouchOutside(false);
        Dialog.isVisible = true;

        Dialog.quitButton.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                updateHighScore(gameController.getPoints());
                GameActivity.this.finish();
            }
        });

        Dialog.tryAgainButton.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                updateHighScore(gameController.getPoints());
                recreate();
            }
        });

    }

    private void updateHighScore(int points){
        if(points > highScore){
            highScore = points;

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt(getString(R.string.high_score), highScore);
            editor.commit();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event){
        float x = event.getX(), y = event.getY();
        Log.d("TOUCH", "Selected: [ " + x + ", " + y + " ]");

        switch (event.getActionMasked()){
            case MotionEvent.ACTION_DOWN:
                gameController.down(x, y);
                break;
            case MotionEvent.ACTION_CANCEL:
                gameController.cancel();
                break;
        }

        return true;
    }

    @Override
    public void finish(){
        super.finish();
        updateHighScore(gameController.getPoints());
    }

    @Override
    public void updateImage() {
        imageView.invalidate();
    }

    @Override
    public void addScore(int points) {
        ((TextView)findViewById(R.id.scoreText)).setText("Score: " + points);
    }

    @Override
    public void updateNewPieces(){
        ((ImageView)findViewById(R.id.firstNextPiece)).setImageBitmap(imageData.getNextPiece(0));
        ((ImageView)findViewById(R.id.secondNextPiece)).setImageBitmap(imageData.getNextPiece(1));
        ((ImageView)findViewById(R.id.thirdNextPiece)).setImageBitmap(imageData.getNextPiece(2));
    }
}
