package com.example.milani.lines.game;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;

/**
 * Created by milani on 26.6.18..
 */

public class Field{

    boolean selected;
    Piece piece;
    int positionI, positionJ;
    RectF position;

    public Field(int i, int j) {
        positionI = i;
        positionJ = j;
    }

    public boolean setPiece(Piece piece){
        if(piece != null) {
            this.piece = piece;
            return true;
        }
        return false;
    }

    public void draw(Canvas canvas){
        if(piece != null && piece.bitmap != null)
            canvas.drawBitmap(piece.bitmap, null, position, new Paint());

        if(selected) {
            Path path = new Path();
            path.moveTo(position.left, position.top);
            path.lineTo(position.left, position.bottom);
            path.lineTo(position.right, position.bottom);
            path.lineTo(position.right, position.top);
            path.lineTo(position.left, position.top);
            path.close();

            Paint paint = new Paint();
            paint.setColor(0x00ff00);
            paint.setAlpha(60);
            canvas.drawPath(path, paint);
        }
    }

    public boolean equals(Field f){
        return f.getPiece() == null && this.piece == null || // no one has a figure
                f.getPiece().bitmapIndex == this.piece.bitmapIndex; // both have a piece with the same figure
    }

    public boolean hasSamePieceType(int pieceType){
        return this.getPiece() != null && this.getPiece().bitmapIndex == pieceType;
    }


    public void setPosition(RectF position) { this.position = position; }

    public Piece getPiece() {
        return piece;
    }

    public int getI() { return positionI; }
    public int getJ() { return positionJ; }
}
