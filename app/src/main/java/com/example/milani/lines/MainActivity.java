package com.example.milani.lines;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void continueGame(View view) {
    }

    public void startNewGame(View view) {
        Intent gameIntent = new Intent(this, GameActivity.class);
        // best score and other args

        startActivity(gameIntent);
    }

    public void showStatistics(View view) {
    }

    public void showSettings(View view) {
    }
}
