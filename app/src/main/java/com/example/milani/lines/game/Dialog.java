package com.example.milani.lines.game;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by milani on 27.6.18..
 */

public class Dialog {

    public static AlertDialog dialog;
    public static TextView scoreTextView;
    public static ImageView imageView;
    public static Button tryAgainButton;
    public static Button quitButton;
    public static boolean isVisible;

    public static void show(){ if(isVisible)dialog.show(); }
    public static void setScore(int score) { scoreTextView.append((new Integer(score).toString())); }

    public static void setDialog(final AlertDialog alertDialog){
        dialog = alertDialog;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
}
